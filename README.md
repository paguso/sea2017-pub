## Online Construction of Wavelet Tres (Supplementary material)
### Paulo GS Fonseca and Israel BF Silva
### 16th Intl Symposium on Experimental Algorithms - SEA 2017
### King's College London, UK, June 21-23, 2017

----

## Experiments Cookbook

Here are the minimal instructions on how to reproduce the experiments described
in the paper.

### What you need
- Computer
- Internet
- Linux (any Unix-based OS should do. No idea about Windows.)
- Bash shell
- Git
- wget
- GCC
- Cmake
- Python
- Valgrind
- GNUPlot

### 0. Clone this repository
```
git clone https://www.gitlab.com/paguso/sea2017-pub.git
```
From now on, all the commands assume that you are in the directory `sea2017-pub`.

### 1. Build the software
```
cd sw/claude[fs,gog,sepulveda,shun]
make install
```

### 2. Download the base data
```
cd pizzachili
sh ./downloadall.sh
```
*WARNING*: This will take time and will consume 4GB of disk space.

### 3. Build the test data
```
./runall.sh builddata all
```

### 4. Run the time tests
```
./runall.sh runtime all all 1
```
*WARNING*: This will take some time.

### 5. Run the memory tests
```
./runall.sh runmem all all 1
```
*WARNING*: This will take a bit longer yet.


### 6. Process the results
```
./runall.sh process all all
```

Voilà, the raw data and results (.dat, .tex, .gnuplot, .pdf) are in the 
`results` folder.

### Getting help

```
./runall.sh help
```

## Graphical Simulator

A Java graphical simulator of the algorithm (JFSSim), for demonstration purposes only, is included in the ```jfssim``` folder.

