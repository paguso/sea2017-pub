
## Online Construction of Wavelet Tres (Supplementary material)
### Paulo GS Fonseca and Israel BF Silva
### 16th Intl Symposium on Experimental Algorithms - SEA 2017
### King's College London, UK, June 21-23, 2017

----

## JFSSim Graphical Simulator

A Java graphical simulator of the algorithm (JFSSim), for demonstration purposes only, is included here.

To run, just download the ```JFSSim.jar``` file locaded in de ```bin``` folder and run

```
java -jar JFSSim.jar
```

Then click "New" to select the input text file, and then ">" to build the WT automatically, or ">|" to build it step-by-step.


