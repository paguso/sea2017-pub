#include <stddef.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

static const size_t MAX_BUF = ((size_t)0x1)<<20;

#define MIN(A, B) (((A)<(B))?(A):(B))

void help()
{
    printf("\nUsage: crop <input file> <start pos> <nb of bytes> <output file>\n");
    printf("\nThe start position and number of bytes can be written\n");
    printf("either as a plain decimal integer with no spaces or comma\n");
    printf("e.g. 1024, 134217728, 8388608\n");
    printf("or in a more human-readable format using the suffixes \n");
    printf("K (kilo), M (mega) or G (giga) \n");
    printf("e.g. 1K (=1024), 128M (=134217728), 8G (=8388608)\n");
	printf("For the <nb of bytes> you can also use ALL to indicate that the file\n");
	printf("should be read until its end\n\n");
}

size_t atos(char *str)
{
    int l = strlen(str);
    char last = str[l-1];
    size_t mult = 0;
    if (last=='K' || last=='k')
        mult = 10;
    else if (last=='M' || last=='m')
        mult = 20;
    else if (last=='G' || last=='g')
        mult = 30;
    size_t ret = 0;
    size_t b = 1;
    for (int i=strlen(str)-((mult>0)?2:1); i>=0; i--) {
        ret += ((size_t)(str[i]-'0'))*b;
        b*=10;
    }
    return ret<<mult;
}

size_t crop(FILE *in, size_t start, size_t nbytes, FILE *out)
{
    size_t bufsize = MAX_BUF;
    void *buf = malloc(bufsize);
    fseek(in, start, SEEK_SET);
    size_t cur=start, end=start+nbytes, n;
    while (!feof(in) && cur<end) {
        n = MIN(bufsize, (end-cur));
        n = fread(buf, 1, n, in);
        fwrite(buf, 1, n, out);
        cur += n;
		//printf(" > read %zu bytes current position is %zu\n",n, cur);
        printf(".");
    }
	printf("\n");
    free(buf);
    return cur-start;
}

int main(int argc, char *argv[])
{
    if (argc<2) {
        help();
        exit(1);
    }
    if (!strcmp(argv[1], "-h")
        || !strcmp(argv[1], "--help")) {
        help();
        exit(1);
    }
    FILE *in = fopen(argv[1], "r");
    size_t start = atos(argv[2]);
    size_t nbytes;
	if (!strcmp(argv[3], "ALL")) 
		nbytes = SIZE_MAX;
	else 
		nbytes = atos(argv[3]);
    FILE *out	= fopen(argv[4], "w");
    printf ("cropping %s from position %zu to %zu into %s\n", argv[1], start,
            start+nbytes, argv[4]);
    size_t n = crop(in, start, nbytes, out);
    printf ("%zu bytes written\n", n);
    fclose(in);
    fclose(out);
    exit(0);
}
