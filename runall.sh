#!/bin/bash

#DATAINPUTPATH="./pizzachili"
DATAINPUTPATH="../pizza-chili-corpus"
#ALLDATAIDS=("dna" "proteins" "xml" "sources" "english") 
#ALLDATASRCFILES=("dna" "proteins" "dblp.xml" "sources" "english") 
ALLDATAIDS=("english") 
ALLDATASRCFILES=("english") 
ALPHABETSIZEFILE="./alphabet_sizes.dat"
CROPCMD="./crop"
#SIZES=(02M 04M 08M 16M 32M 64M 128M)
SIZES=(ALL)
DATAOUTPUTPATH="./data"
DATAIDS=()
DATASRCFILES=() 

#ALLALGIDS=("fs" "libcds" "sdslil" "sdslrrr" "pwt" "dd" "swt")
#ALLALGCMD=("./sw/fs/fs2" "./sw/claude/claude" "./sw/gog/gog-il" "./sw/gog/gog-rrr" "./sw/sepulveda/sepulveda-pwt" "./sw/sepulveda/sepulveda-dd" "./sw/shun/shun")
ALLALGIDS=("fs" "sdslil" "sdslrrr" "pwt" "dd" "swt")
ALLALGCMD=("./sw/fs/fs2" "./sw/gog/gog-il" "./sw/gog/gog-rrr" "./sw/sepulveda/sepulveda-pwt" "./sw/sepulveda/sepulveda-dd" "./sw/shun/shun")
EXPOUTPUTPATH="./results"
ALGIDS=()
ALGCMD=()

TIMEREPS=1
MEMREPS=1
CSVSEP=" "

KEEPMASSIFOUT=false

printhelp() {
	echo ""
    echo "Usage   : runall.sh <action> [<options>]"
    echo "Actions : builddata - compile input data set"
    echo "          runtime - run algorithm and collect execution time data"
    echo "          runmem - run algorithm and collect memory usage data"
    echo "          process - produce summarised output from experimental data"
    echo "          clean - clean previous results"
    echo "          help - print this message"
	echo
    echo "Each action may take specific arguments as follows:"
    echo ""
    echo "- runall.sh builddata <data_id>"
    echo ""
    echo "  . <data_id> identifies the data set for which input files will be" 
    echo "       generated."
    echo "       Possible values are obtained from the \$ALLALGIDS  array."
    echo "       Current values are "
	for dtid in ${ALLDATAIDS[@]}; do
	echo "           . $dtid"
	done
    echo "       The value 'all' will create input files for all data_ids."
    echo "       For each data_id, a set of files will be created and put into "
    echo "       \$DATAOUTPUTPATH."
    echo "       Each file will correspond to a size specified in the \$SIZES array."
    echo "       More specifically, for each data_id, the script will use the "
    echo "       provided program 'crop' to create a file with each defined size "
    echo "       from a corresponding source file given in \$ALLDATASRCFILES."
    echo ""
    echo "- runall.sh runtime <alg_id> <data_id> <repetitions>"
    echo "- runall.sh runmem <alg_id> <data_id> <repetitions>"
    echo ""
    echo "  . <data_id> identifies the data set used as input for running "
	echo "       the algorithms." 
    echo "  . <alg_id> specifies the algorithm to be tested for runtime/memory usage."
    echo "       Possible values are obtained from \$ALLALGIDS."
    echo "       Current  values are "
	for algid in ${ALLALGIDS[@]}; do
	echo "            . $algid"
	done
    echo "       The value 'all' means that all algorithms should be executed."
    echo "       For each alg_id, the corresponding command from the \$ALLALGCMD "
    echo "       array is called to  run the program on each input file previously "
    echo "       created for the given <data_id>. Giving 'all' for <data_id> causes" 
    echo "       the algorithm(s) to be run on all data files."
    echo "       Output files with the corresponding execution times / memory usage "
    echo "       data are written to the \$EXPOUTPUTPATH path. "
    echo "  . <repetitions> indicates the number of times the procedure must be repeated."
	echo ""
    echo "- runall.sh process <data_id> <data_type>"
    echo "- runall.sh clean <data_id> <data_type>"
    echo ""
    echo "  . <data_id> identifies the data set whose results will be "
	echo "       deleted / processed." 
	echo "  . <data_type> is either 'time', 'mem' or 'all' and indicates "
	echo "       which data will be deleted / processed.  "
    echo ""
}

setdata() {
	echo "Setting data..."
	DATAIDS=()
	DATASRCFILES=() 
	for ((i=0; i<${#ALLDATAIDS[@]}; i++));
	do
		if [ $1 = "all" ] || [ $1 = ${ALLDATAIDS[$i]} ]; then
			echo "    Add source: ${ALLDATASRCFILES[$i]}"
			DATAIDS+=(${ALLDATAIDS[$i]})
			DATASRCFILES+=(${ALLDATASRCFILES[$i]})
		fi
	done
	echo "Done."
}


setalg() {
	echo "Setting programs..."
	ALGIDS=()
	ALGCMD=() 
	for ((i=0; i<${#ALLALGCMD[@]}; i++));
	do
		if [ $1 = "all" ] || [ $1 = ${ALLALGIDS[$i]} ]; then
			echo "    Add program: ${ALLALGCMD[$i]}"
			ALGIDS+=(${ALLALGIDS[$i]})
			ALGCMD+=(${ALLALGCMD[$i]})
		fi
	done
	echo "Done."
}


clean() {
    echo "Cleaning results..."
	local dataid
	for ((i=0; i<${#DATAIDS[@]}; i++));
	do
		dataid=${DATAIDS[$i]}
		if [ -d $EXPOUTPUTPATH ]; then
			if [[ $1 = 'time' || $1 = 'all' ]]; then
				rm $EXPOUTPUTPATH/${dataid}.time
				rm $EXPOUTPUTPATH/${dataid}-time.csv
				rm $EXPOUTPUTPATH/${dataid}-time.dat
				rm $EXPOUTPUTPATH/${dataid}-time.gnuplot
				rm $EXPOUTPUTPATH/${dataid}-time.pdf
				rm $EXPOUTPUTPATH/${dataid}-time.tex
			fi
			if [[ $1 = 'mem' || $1 = 'all' ]]; then
				rm $EXPOUTPUTPATH/${dataid}.mem
				rm $EXPOUTPUTPATH/massif.out.*${dataid}*
				rm $EXPOUTPUTPATH/${dataid}-mem.csv
				rm $EXPOUTPUTPATH/${dataid}-mem.dat
				rm $EXPOUTPUTPATH/${dataid}-mem.gnuplot
				rm $EXPOUTPUTPATH/${dataid}-mem.pdf
				rm $EXPOUTPUTPATH/${dataid}-mem.tex
				rm $EXPOUTPUTPATH/${dataid}-mem-over.csv
			fi
		fi
	done
	echo "Done."
}


check_crop() {
	if [ ! -x $CROPCMD ]; then
		echo "Crop tool not found!"
		echo "Will try to build it..."
		gcc -O3 -o crop crop.c
		CROPCMD="./crop"
		echo "OK!"
	fi
}


builddata() {
	check_crop
	echo "Building data files..."
	local dataid src dest
	mkdir -p $DATAOUTPUTPATH
	for ((i=0; i<${#DATAIDS[@]}; i++));
	do
		dataid=${DATAIDS[$i]}
		src=$DATAINPUTPATH/${DATASRCFILES[$i]}
		if [ ! -f $src ]; then
			echo "    ERROR: Input file $src not found."
		    echo "           Skipping this one."
		else 
			echo "    Reading source file:" $src
			local slen
			slen=${#SIZES[@]}
			for ((s=0; s<$slen; s++)); 
			do
				dest=$DATAOUTPUTPATH/$dataid-${SIZES[$s]}
				$CROPCMD $src 0 ${SIZES[$s]} $dest
				echo "        Created file:" $src
			done
		fi
	done
	echo "Done."
}


runtimeexp() {
	echo "Running time experiments..."
	local algid algcmd dataid datafile resultfile
	#echo "creating $EXPOUTPUTPATH"
	mkdir -p $EXPOUTPUTPATH
	for ((r=0; r<$1; r++)); 
	do
		for ((a=0; a<${#ALGIDS[@]}; a++)); 
		do
			algid=${ALGIDS[$a]}
			algcmd=${ALGCMD[$a]}
			for ((d=0; d<${#DATAIDS[@]}; d++)); 
			do	
				dataid=${DATAIDS[$d]} 
				#echo "dataid=$dataid"
				for ((s=0; s<${#SIZES[@]}; s++)); 
				do
					datafile=$DATAOUTPUTPATH/$dataid-${SIZES[$s]}
					#resultfile=$EXPOUTPUTPATH/$algid-$dataid-${SIZES[$s]}
					resultfile=$EXPOUTPUTPATH/$dataid.time
					echo "- Running $algcmd $datafile"	
					timestamp=`date +%Y%m%d%H%M%S%Z`
					{ TIMEFORMAT="${timestamp}${CSVSEP}${dataid}${CSVSEP}${SIZES[$s]}${CSVSEP}${algid}${CSVSEP}%3R" ; time $algcmd $datafile; } 2>> $resultfile 
					touch $resultfile
				done
			done
		done
	done
	echo "Done."
}

runmemexp() {
	echo "Running memory experiments..."
	local algid algcmd dataid datafile resultfile
	#echo "creating $EXPOUTPUTPATH"
	mkdir -p $EXPOUTPUTPATH
	for ((r=0; r<$1; r++)); 
	do
		for ((a=0; a<${#ALGIDS[@]}; a++)); 
		do
			algid=${ALGIDS[$a]}
			algcmd=${ALGCMD[$a]}
			for ((d=0; d<${#DATAIDS[@]}; d++)); 
			do	
				dataid=${DATAIDS[$d]} 
				#echo "dataid=$dataid"
				for ((s=0; s<${#SIZES[@]}; s++)); 
				do
					datafile=$DATAOUTPUTPATH/$dataid-${SIZES[$s]}
					#resultfile=$EXPOUTPUTPATH/$algid-$dataid-${SIZES[$s]}
					resultfile=$EXPOUTPUTPATH/$dataid.mem
					echo "- Running $algcmd $datafile"	
					timestamp=`date +%Y%m%d%H%M%S%Z`
					massifout=$EXPOUTPUTPATH/massif.out.$timestamp-$algid-$dataid-${SIZES[$s]}
					valgrind --tool=massif --massif-out-file=$massifout $algcmd $datafile
					#mv $massifout $EXPOUTPUTPATH
					echo "${timestamp}${CSVSEP}${dataid}${CSVSEP}${SIZES[$s]}${CSVSEP}${algid}${CSVSEP}"`python mempeak.py $massifout`>>$resultfile
					if [ ! $KEEPMASSIFOUT ]; then
						rm $massifout
					fi
					touch $resultfile
				done
			done
		done
	done
	echo "Done."
}

process() {
    echo "Processing results..."
	local dataid
	for ((i=0; i<${#DATAIDS[@]}; i++));
	do
		dataid=${DATAIDS[$i]}
		if [[ $1 = 'time' || $1 = 'all' ]]; then
			python process_results.py $ALPHABETSIZEFILE $EXPOUTPUTPATH/${dataid}.time time $EXPOUTPUTPATH/$dataid
		fi
		if [[ $1 = 'mem' || $1 = 'all' ]]; then
			python process_results.py $ALPHABETSIZEFILE $EXPOUTPUTPATH/${dataid}.mem mem $EXPOUTPUTPATH/$dataid
		fi
	done
	echo "Done."
}


action=$1
case $action in
"clean")
    if [ $# -ne 3 ]; then
        printhelp
        exit 1
    fi
    setdata $2
    clean $3
;;
"builddata")
    if [ $# -ne 2 ]; then
        printhelp
        exit 1
    fi
    setdata $2
    builddata
;;
"runtime")
    if [ $# -ne 4 ]; then
        printhelp
        exit 1
    fi
	setalg $2
    setdata $3
	runtimeexp $4
;;
"runmem")
    if [ $# -ne 4 ]; then
        printhelp
        exit 1
    fi
	setalg $2
    setdata $3
	runmemexp $4
;;
"process")
    if [ $# -ne 3 ]; then
        printhelp
        exit 1
    fi
    setdata $2
	process $3
;;
"help")
    printhelp
;;
esac

