// 
// How to run: make -f Makesdsl && ./main dna.txt 
// 
// Note: Change to plain bitvector. RRR compress data!
// 

#include <sdsl/wavelet_trees.hpp>
#include <iostream>

using namespace sdsl;
using namespace std;

int main(int argc, char* argv[]) {
	if (argc < 2) {
		cout << "Usage: " << argv[0] << " file" << endl;
		return 1;
	}
	wt_blcd<rrr_vector<63>> wt;
	construct(wt, argv[1], 1);
}

