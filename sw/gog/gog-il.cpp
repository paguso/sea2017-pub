#include <sdsl/wavelet_trees.hpp>
#include <iostream>

using namespace sdsl;
using namespace std;

int main(int argc, char* argv[]) {
	if (argc < 2) {
		cout << "Usage: " << argv[0] << " file" << endl;
		return 1;
	}
	wt_blcd<bit_vector_il<512>> wt;
	construct(wt, argv[1], 1);
}

