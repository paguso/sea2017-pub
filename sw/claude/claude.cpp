// 
// How to run: make -f Makelibcds2 && ./main
// 

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <vector>

#include <libcds2/array.h>
#include <libcds2/immutable/bitsequence.h>
#include <libcds2/immutable/bitsequenceonelevelrank.h>
#include <libcds2/immutable/mappernone.h>
#include <libcds2/immutable/wavelettreenoptrs.h>
#include <libcds2/libcds.h>

using cds::basic::Array;
using cds::basic::cds_word;
using cds::immutable::BitSequence;
using cds::immutable::BitSequenceBuilder;
using cds::immutable::BitSequenceBuilderOneLevelRank;
using cds::immutable::BitSequenceOneLevelRank;
using cds::immutable::MapperNone;
using cds::immutable::Sequence;
using cds::immutable::WaveletTreeNoPtrs;

using std::vector;
using std::cout;
using std::endl;

// Note: cds_word = size_t

void fromFile1(char *fn) {
	FILE* fp = fopen(fn, "r");
	size_t read;
	
	if (!fp) {
		fprintf(stderr, "Error opening file \"%s\".\n", fn);
		exit(-1);
	}
	
	fseek(fp, 0L, SEEK_END);
	unsigned long n = (unsigned long) ftell(fp);
	Array *arr = Array::Create(n, 8); // 8 bits per element

	fseek(fp, 0L, SEEK_SET);

	char* t;
	t = (char*) malloc((n + 1) * sizeof(char));
	read = fread(t, sizeof(char), n, fp);
	if(read != n) {
		fprintf(stderr, "Error reading file \"%s\".\n", fn);
		exit(-1);
	}

	t[n] = '\0';
	fclose(fp);
	
	// Input
	for(unsigned long i = 0; i < n; ++i)
		arr->SetField(i, t[i]);
	
	free(t);
	
	// Construction
	new WaveletTreeNoPtrs(arr,
		new BitSequenceBuilderOneLevelRank(20),
		new MapperNone());
}

// This version does not load the input to memory
// It is marginally slower though.
// However, the construction is already taking
// much more space than the input sequence anyway.
void fromFile2(char *filename) {

    FILE *fp;
    int c;

    if (!(fp = fopen(filename, "rt"))) {
        perror(filename);
        exit(1);
    }
	fseek(fp, 0L, SEEK_END);
	unsigned long n = (unsigned long) ftell(fp);
	fseek(fp, 0L, SEEK_SET);

	Array *arr = Array::Create(n, 8); // 8 bits per element
	unsigned long i=0;
    while ((c = fgetc(fp)) != EOF) {
		arr->SetField(i++, (char)c);
    }
    fclose(fp);
	assert(i==n);
	// Construction
	new WaveletTreeNoPtrs(arr,
		new BitSequenceBuilderOneLevelRank(20),
		new MapperNone());
}



int main(int argc, char **argv) {
	if (argc < 2) {
		cout << "Usage: " << argv[0] << " file" << endl;
		return 1;
	}
	fromFile1(argv[1]);
	return 0;
}

