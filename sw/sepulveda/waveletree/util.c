#include <stdio.h>
#include <stdlib.h>
#include "util.h"

symbol* read_text_from_file(const char* fn, unsigned long* n) {

  FILE* fp = fopen(fn, "r");
  size_t read;

  if (!fp) {
    fprintf(stderr, "Error opening file \"%s\".\n", fn);
    exit(-1);
  }

  fseek(fp, 0L, SEEK_END);
  *n = (unsigned long)ftell(fp);

  symbol* t;
  t = (symbol*) malloc((*n + 1) * sizeof(symbol));

  *n = *n / sizeof(symbol); // Number of symbols

  fseek(fp, 0L, SEEK_SET);

  read = fread(t, sizeof(symbol), *n, fp);
  if(read != *n){
    fprintf(stderr, "Error reading file \"%s\".\n", fn);
    exit(-1);
  }

  t[n[0]] = '\0';
  printf("   n: %lu\n", n[0]);
  printf(">>>>> %s\n", t);
  fclose(fp);

  return t;
}

symbol* read_text_from_ascii_file(const char* fn, unsigned long* n,
	unsigned int *alphabet, unsigned char *tableOne, unsigned char *tableTwo) {

  FILE* fp = fopen(fn, "r");
  size_t read;

  if (!fp) {
    fprintf(stderr, "Error opening file \"%s\".\n", fn);
    exit(-1);
  }

  fseek(fp, 0L, SEEK_END);
  *n = (unsigned long) ftell(fp); // Number of symbols

  symbol* t;
  t = (symbol*) malloc((*n + 1) * sizeof(symbol));

  //*n = *n / sizeof(symbol);

  fseek(fp, 0L, SEEK_SET);

  read = fread(t, sizeof(symbol), *n, fp);
  if(read != *n){
    fprintf(stderr, "Error reading file \"%s\".\n", fn);
    exit(-1);
  }

  t[*n] = '\0';
  
  // Transform t[] to symbols [0..sigma-1]
  tableOne = (unsigned char*) calloc(256, sizeof(unsigned char));
  unsigned long i;
  for(i = 0; i < *n; ++i)
  	tableOne[t[i]] = 1;
  
  tableTwo = (unsigned char*) malloc(256 * sizeof(unsigned char));
  for(i = 0; i < 256; ++i)
    tableTwo[i] = -1;
  
  *alphabet = 0;
  for(i = 0; i < 256; ++i) {
    if(tableOne[i] > 0) {
    	tableOne[i] = *alphabet;
    	tableTwo[*alphabet] = i;
    	++*alphabet;
    } else
    	tableOne[i] = -1;
  }
  
  for(i = 0; i < *n; ++i)
  	t[i] = tableOne[t[i]];
  
  // Adjusting |alphabet| to be 2^k
  while(*alphabet != (*alphabet & (- *alphabet)))
  	*alphabet += *alphabet & (- *alphabet);
  
  //printf("   n: %lu\n", n[0]);
  //printf(">>>>> %s\n", t);
  fclose(fp);

  return t;
}


