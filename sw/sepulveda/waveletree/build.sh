#opt=-O2 default opt
opt=-O3
#def="-DNO_RANK_SELECT"
def="-DNO_CILK -DCHARS"

cd bitrank
gcc $debug -std=gnu89 -c *.c

cd ..

echo "Compiling per level algorithm"
gcc $opt $def -std=gnu89 -c pwt.c util.c -lrt -fcilkplus -lcilkrts -lm -g
gcc $opt $def -std=gnu89 pwt.o util.o bitrank/basic.o bitrank/bit_array.o bitrank/bitrankw32int.o -o pwt -lrt -fcilkplus -lcilkrts -lm -g

echo "Compiling domain decomposition algorithm"
gcc $opt $def -std=gnu89 -c dd.c util.c -lrt -fcilkplus -lcilkrts -lm -g
gcc $opt $def -std=gnu89 dd.o util.o bitrank/basic.o bitrank/bit_array.o bitrank/bitrankw32int.o -o dd -lrt -fcilkplus -lcilkrts -lm -g

echo "Done."
