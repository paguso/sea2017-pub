#include <stdlib.h>
#include <stdio.h>

#include <cocada/bytearray.h>
#include <cocada/cstringutil.h>
#include <cocada/strstream.h>
#include <cocada/wavtree.h>

void online(char *filename) {
	strstream *sst; 
	sst = strstream_open_file(filename);
	wavtree *wt;
	printf("creating wavelet_tree (online)\n");
	wt = wavtree_new_online_from_stream(sst, WT_BALANCED);
	printf("wavelet_tree created\n");
	//strstream_close(sst);
	//wavtree_free(wt);
}

int main(int argc, char *argv[])
{
	online(argv[1]);
	return 0;
}
