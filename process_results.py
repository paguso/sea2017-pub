import math, numpy, sys, subprocess

def sizeid_to_int(size):
    return int(size[:len(size)-1])


def process_time(filename):
    resultfile = open(filename, "r")
    points = {}
    for line in resultfile:
        [timestamp, dataid, sizeid, algid, xtime] = line.split()
        xtime = float(xtime)
        #print dataid, sizeid, algid, xtime
        if dataid not in points:
            points[dataid] = {}
        datapts = points[dataid]
        size = sizeid_to_int(sizeid)
        if size not in datapts:
            datapts[size] = {}
        datasizepts = datapts[size]
        if algid not in datasizepts:
            datasizepts[algid] = []
        datasizealgpts = datasizepts[algid]
        datasizealgpts.append(xtime)
    resultfile.close()
    results = []
    for dataid in points:
        for size in points[dataid]:
            for algid in points[dataid][size]:
                pts = points[dataid][size][algid] 
                avg = sum(pts)/len(pts)
                std = numpy.std(pts)
                #print "average of ", dataid, sizeid, algid, "=", avg 
                results.append((dataid, size, algid, avg, std))
    results.sort()
    #print results
    return results

def process_mem(filename):
    resultfile = open(filename, "r")
    points = {}
    for line in resultfile:
        [timestamp, dataid, sizeid, algid, xmem] = line.split()
        xmem = float(xmem)
        #print dataid, sizeid, algid, xmem
        if dataid not in points:
            points[dataid] = {}
        datapts = points[dataid]
        size = sizeid_to_int(sizeid)
        if size not in datapts:
            datapts[size] = {}
        datasizepts = datapts[size]
        if algid not in datasizepts:
            datasizepts[algid] = []
        datasizealgpts = datasizepts[algid]
        datasizealgpts.append(xmem/(2**20))
    resultfile.close()
    results = []
    for dataid in points:
        for size in points[dataid]:
            for algid in points[dataid][size]:
                pts = points[dataid][size][algid] 
                avg = sum(pts)/len(pts)
                std = numpy.std(pts)
                #print "average of ", dataid, sizeid, algid, "=", avg 
                results.append((dataid, size, algid, avg, std))
    results.sort()
    #print results
    return results


def output_time_csv(results, outputfnprefix):
    outfn = outputfnprefix+"-time.dat"
    out = open(outfn, "w")
    cols = list(set([x[2] for x in results]))
    cols.sort()
    ncols = len(cols)
    (dl,sl,al,tl,vl) = (None, None, None, None, None)
    j = 0
    for r in results:
        (d,s,a,t,v) = r # (d)ata, (s)ize, (a)lgorithm, avg (t)ime, std.de(v)iation
        if d != dl: # new table
            out.write("#Execution times for data set '%s'\n"%(d))
            out.write("#size\t%s"%("\t".join(cols)))
        if s != sl: # new size, break line
            out.write ("\n%d"%s)
            j = 0 
        while a!=cols[j]:
            out.write("\t?\t?")
            j+=1
        out.write("\t%f\t%f"%(t,v))
        j+=1
        (dl,sl,al,tl,vl) = (d,s,a,t,v)
    out.close()


def output_mem_csv(results, outputfnprefix):
    outfn = outputfnprefix+"-mem.dat"
    out = open(outfn, "w")
    cols = list(set([x[2] for x in results]))
    cols.sort()
    ncols = len(cols)
    (dl,sl,al,ml,vl) = (None, None, None, None, None)
    j = 0
    for r in results:
        (d,s,a,m,v) = r # (d)ata, (s)ize, (a)lgorithm, avg (m)emory, std.de(v)iation
        if d != dl: # new table
            out.write("#Memory usage for data set '%s'\n"%(d))
            out.write("#size\t%s"%("\t".join(cols)))
        if s != sl: # new size, break line
            out.write ("\n%d"%s)
            j = 0 
        while a!=cols[j]:
            out.write("\t?\t?")
            j+=1
        out.write("\t%f\t%f"%(m,v))
        j+=1
        (dl,sl,al,ml,vl) = (d,s,a,m,v)
    out.close()


def output_time_latex(results, outputfnprefix):
    datfn = outputfnprefix+"-time.dat"
    dat = open(datfn, "r")
    outfn = outputfnprefix+"-time.tex"
    out = open(outfn, "w")
    title = dat.readline().strip()[1:]
    algs = dat.readline().strip().split()[1:]
    nalgs = len(algs)
    out.write("\\begin{table}\n")
    out.write("\\begin{tabular}{| c | %s |}\n"%(" ".join(nalgs*['c'])))
    out.write("\\hline\n")
    out.write("\\multirow{2}{*}{Size} & \\multicolumn{%d}{c|}{Algorithms}\\\\\n"%nalgs)
    out.write("&"+ " & ".join(algs) + "\\\\\n")
    out.write("\hline\n")
    for line in dat:
        tokens = line.strip().split('\t')
        cells = [tokens[0]]
        cells.extend([tokens[i]+" ("+tokens[i+1]+")" for i in range(1,len(tokens),2)])
        out.write(" & ".join(cells) + "\\\\\n")
    out.write("\\hline\n")
    out.write("\\end{tabular}\n")
    out.write("\\caption{%s}\label{fig:%s}\n"%(title, title.split("'")[1]+"-time"))
    out.write("\\end{table}")
    dat.close()
    out.close()


def output_mem_latex(results, outputfnprefix):
    datfn = outputfnprefix+"-mem.dat"
    dat = open(datfn, "r")
    outfn = outputfnprefix+"-mem.tex"
    out = open(outfn, "w")
    title = dat.readline().strip()[1:]
    algs = dat.readline().strip().split()[1:]
    nalgs = len(algs)
    out.write("\\begin{table}\n")
    out.write("\\begin{tabular}{| c | %s |}\n"%(" ".join(nalgs*['c'])))
    out.write("\\hline\n")
    out.write("\\multirow{2}{*}{Size} & \\multicolumn{%d}{c|}{Algorithms}\\\\\n"%nalgs)
    out.write("&"+ " & ".join(algs) + "\\\\\n")
    out.write("\hline\n")
    for line in dat:
        tokens = line.strip().split('\t')
        cells = [tokens[0]]
        #cells.extend([tokens[i]+" ("+tokens[i+1]+")" for i in range(1,len(tokens),2)])
        cells.extend([tokens[i] for i in range(1,len(tokens),2)])
        out.write(" & ".join(cells) + "\\\\\n")
    out.write("\\hline\n")
    out.write("\\end{tabular}\n")
    out.write("\\caption{%s}\label{fig:%s}\n"%(title, title.split("'")[1]+"-mem"))
    out.write("\\end{table}")
    dat.close()
    out.close()


def maxY(results):
    Y = [ x[-2] for x in results]
    Y.sort()
    #Y.pop(-1)
    maxY = int(numpy.average(Y)+numpy.std(Y))
    return maxY

def output_time_gnuplot(results, outputfnprefix):
    datfn = outputfnprefix+"-time.dat"
    dat = open(datfn, "r")
    outfn = outputfnprefix+"-time.gnuplot"
    out = open(outfn, "w")
    title = dat.readline().strip()[1:]
    algs = dat.readline().strip().split()[1:]
    out.write("set terminal pdfcairo size 5,4 color enhanced font 'Helvetica, 16'\n")
    out.write("set title \"%s\" font ',20'\n"%title)
    out.write("set xlabel 'Input size (in Mbytes)' font ',18'\n")
    out.write("set ylabel 'Execution time (in seconds)' font ',18'\n")
    #out.write("set logscale xy 2\n")
    out.write("set yrange[0:%d]\n"%maxY(results))
    out.write("set key inside top left font ',16'\n")
    #out.write("set key below font ',14'\n")
    out.write("set output '%s'\n"%(outputfnprefix+"-time.pdf"))
    out.write("plot '%s' using 1:2:xtic(1) title '%s' with linespoints"%(datfn, algs[0]))
    for k in range(1, len(algs)):
        out.write(",\\\n     '%s' using 1:%d title '%s' with linespoints"%(datfn, 2*k+2, algs[k]))
    out.write("\n")
    out.write("unset output")
    dat.close()
    out.close()
    subprocess.call(["gnuplot", "-c", outfn])


def output_mem_gnuplot(results, outputfnprefix):
    datfn = outputfnprefix+"-mem.dat"
    dat = open(datfn, "r")
    outfn = outputfnprefix+"-mem.gnuplot"
    out = open(outfn, "w")
    title = dat.readline().strip()[1:]
    algs = dat.readline().strip().split()[1:]
    out.write("set terminal pdfcairo size 5,4 color enhanced font 'Helvetica, 16'\n")
    out.write("set title \"%s\" font ',20'\n"%title)
    out.write("set xlabel 'Input size (in Mbytes)' font ',18'\n")
    out.write("set ylabel 'Memory usage (in Mbytes)' font ',18'\n")
    #out.write("set yrange[0:%d]\n"%maxY(results))
    #out.write("set logscale xy 2\n")
    out.write("set key inside top left font ',16'\n")
    out.write("set output '%s'\n"%(outputfnprefix+"-mem.pdf"))
    out.write("plot '%s' using 1:2:xtic(1) title '%s' with linespoints"%(datfn, algs[0]))
    for k in range(1, len(algs)):
        out.write(",\\\n     '%s' using 1:%d title '%s' with linespoints"%(datfn, 2*k+2, algs[k]))
    out.write("\n")
    out.write("unset output")
    dat.close()
    out.close()
    subprocess.call(["gnuplot", "-c", outfn])


def read_alphabet_sizes(alphabetsizefn):
    ablist = []
    absize = {}
    f = open(alphabetsizefn)
    for line in f:
        tokens = line.strip().split()
        ablist.append(tokens[0])
        absize[tokens[0]] = int(tokens[1])
    f.close()
    return ablist, absize


def mem_overhead_csv(ablist, absizes, mem_results, outputfnprefix):
    outfn = outputfnprefix+"-mem-over.csv"
    mem_oh = {}
    j = 0
    for r in mem_results:
        (d,s,a,m,v) = r # (d)ata, (s)ize, (a)lgorithm, avg (m)emory, std.de(v)iation
        #print d,s,a,m,v,
        n = float(s) * (2**20)
        #print "absize=",absizes[d], "log=", math.log(absizes[d],2),
        exp_bits = n * math.log(absizes[d],2)
        #print "exp_bits=",exp_bits,
        act_bits = m*(2**20)*8
        ohead = (act_bits-exp_bits)/exp_bits
        #print "act_bits=",act_bits,"ohead=",ohead
        if d not in mem_oh:
            mem_oh[d] = {}
        if a not in mem_oh[d]:
            mem_oh[d][a] = []
        if ohead>0: # discard negative overheads. valgrind error?
            mem_oh[d][a].append(ohead)
        else:
            print "discarding negative overhead for", a , d, s
    #print mem_oh
    out = open(outfn, "w")
    datasets = mem_oh.keys()
    datasets.sort()
    first = True
    for d in datasets:
        algs = mem_oh[d].keys()
        algs.sort()
        if first:
            out.write("# data_id %s\n"%"\t".join(algs))
            first = False
        out.write(d)
        for a in algs:
            #print mem_oh[d][a]
            out.write("\t%f\t%f"%(numpy.average(mem_oh[d][a]), numpy.std(mem_oh[d][a])))
            #print "mean overhead for ",d,"with", a,"=", numpy.average(mem_oh[d][a])
        out.write("\n")
    out.close()
    return mem_oh


def mem_overhead_tex(ablist, mem_oh, outputfnprefix):
    #print mem_oh
    outfn = outputfnprefix+"-mem-over.tex"
    out = open(outfn, "w")
    datasets = mem_oh.keys()
    first = True
    for d in datasets:
        algs = mem_oh[d].keys()
        algs.sort()
        nalgs = len(algs)
        if first:
            out.write("\\begin{table}\n")
            out.write("\\begin{tabular}{| c | %s |}\n"%(" ".join(nalgs*['c'])))
            out.write("\\hline\n")
            out.write("\\multirow{2}{*}{Size} & \\multicolumn{%d}{c|}{Algorithms}\\\\\n"%nalgs)
            out.write("&"+ " & ".join(algs) + "\\\\\n")
            out.write("\hline\n")
            first = False
        out.write(d)
        for a in algs:
            out.write("& %.2f (%.2f)"%(numpy.average(mem_oh[d][a]), numpy.std(mem_oh[d][a])))
            #print "mean overhead for ",d,"with", a,"=", numpy.average(mem_oh[d][a])
        out.write("\\\\\n")
    out.write("\\hline\n")
    out.write("\\end{tabular}\n")
    out.write("\\caption{%s}\label{tab:memoverheads}\n")
    out.write("\\end{table}")
    out.close()
    return mem_oh


def help():
    print ""
    print "Usage: python process_results.py <absize_file> <input_file> <metric> <output_pref>"
    print "Where:"
    print ". <absize_file> = file containing alphabet sizes"
    print ". <input_file>  = raw data (*.time or *.mem) input file "
    print "                  created by the runall.sh script "
    print ". <metric>      = the type of measurements to process: 'time' or 'mem'"
    print ". <output_pref> = output files prefix. "
    print ""
    print "This program will generate four files:"
    print ". <output_pref>-<metric>.dat     = csv file with a summary of measurements"   
    print ". <output_pref>-<metric>.tex     = LaTeX table with a summary of measurements"
    print ". <output_pref>-<metric>.pdf     = PDF graphic with a summary of measurements"
    print ". <output_pref>-<metric>.gnuplot = GNUPlot script to generate the graphic"
    print ""

def main():
    if len(sys.argv)>1 and sys.argv[1] in ("-h", "-H", "--help", "--Help"):
        help()
        exit(0)
    elif len(sys.argv) != 5:
        help()
        exit(1)
    alphabetsizefn = sys.argv[1]
    inputfn = sys.argv[2]
    metric = sys.argv[3]
    outputfnprefix = sys.argv[4]
    if (metric=="time"):
        time_results = process_time(inputfn)
        output_time_csv(time_results, outputfnprefix)
        output_time_latex(time_results, outputfnprefix)
        output_time_gnuplot(time_results, outputfnprefix)
    elif (metric=="mem"):
        mem_results = process_mem(inputfn)
        output_mem_csv(mem_results, outputfnprefix)
        output_mem_latex(mem_results, outputfnprefix)
        output_mem_gnuplot(mem_results, outputfnprefix)
        ablist, absizes = read_alphabet_sizes(alphabetsizefn)
        mem_oh = mem_overhead_csv(ablist, absizes, mem_results, outputfnprefix)
        mem_overhead_tex(ablist, mem_oh, outputfnprefix)
    else:
        help()
        exit(1)


if __name__ == "__main__":
    main()
