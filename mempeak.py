import sys

'''
Massif output file is as follows
#----------
snapshot=0
#-----------
time=0
mem_heap_B=0
mem_heap_extra_B=0
mem_stacks_B=0
heap_tree=empty
'''

def main():
    if len(sys.argv)!=2:
        print "Usage: python mempeak.py <massif_file>"
        exit(1)

    massif_file = open(sys.argv[1], "r")
    
    mem_heap_B = []
    mem_heap_extra_B = []
    mem_stacks_B = []
    total_heap = []

    line = massif_file.readline()
    while line:
        if line.startswith("mem_heap_B"):
            mem_heap_B.append(int(line.split('=')[1]))    
            line = massif_file.readline()
            assert (line.startswith("mem_heap_extra_B"))
            mem_heap_extra_B.append(int(line.split('=')[1]))    
            total_heap.append(mem_heap_B[-1] + mem_heap_extra_B[-1])
            line = massif_file.readline()
            assert (line.startswith("mem_stacks_B"))
            mem_stacks_B.append(int(line.split('=')[1]))
        line = massif_file.readline()

    #print mem_heap_B
    #print mem_heap_extra_B
    #print mem_stacks_B

    print max(total_heap) if len(total_heap) else 0 

    massif_file.close()


if __name__ == "__main__":
    main()
